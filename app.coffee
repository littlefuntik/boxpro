# include libraries
path   = require 'path'
swig   = require 'swig'
mysql  = require 'mysql'

Application    = require path.resolve __dirname, 'core', 'Application'
IndexCtrl      = require path.resolve __dirname, 'controllers', 'IndexCtrl'
PropCtrl       = require path.resolve __dirname, 'controllers', 'PropCtrl'
TypeCtrl       = require path.resolve __dirname, 'controllers', 'TypeCtrl'

# database connection params
connection = mysql.createConnection
  host: 'localhost'
  port: '/var/run/mysqld/mysqld.sock'
  user: 'root'
  password: '111'
  database: 'boxpro'
  charset: 'UTF8_GENERAL_CI'

# configurations
cfg =
  site_name: 'BoxPro'

  host: '127.0.0.1'
  port: 1337

  # shared url path for directory 'static'
  static_url: 'http://127.0.0.1:1338'

  path:
    views: path.resolve __dirname, 'views'
    static: path.resolve __dirname, 'static'

# template engine default configurations
swig.setDefaults
  loader: swig.loaders.fs cfg.path.views
  cache: false

app = new Application cfg.port, cfg.host

app.templateEngine swig
app.config cfg
app.db connection

app.get '/', IndexCtrl.start
app.get '/api/property', PropCtrl.ls
app.post '/api/property', PropCtrl.mk
app.get /^\/api\/property\/(\d+)$/, PropCtrl.pk
app.post /^\/api\/property\/(\d+)$/, PropCtrl.ee
app.delete /^\/api\/property\/(\d+)$/, PropCtrl.rm
app.get '/api/type', TypeCtrl.ls

do app.run
