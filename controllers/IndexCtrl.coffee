class Controller

  # стартовая страница
  @start: (request, response) ->
    @end @render 'index.html',
      title: @cfg.site_name,
      static_url: @cfg.static_url

module.exports = Controller
