path  = require 'path'
types = require path.resolve __dirname, '..', 'core', 'types'

class Controller

  # список всех свойств
  @ls: (request, response) ->

    res = []

    for id, type of types
      id = parseInt id

      res.push {
        id: id
        name: type.name
      }

    @end res

module.exports = Controller