path   = require 'path'
toJSON = require path.resolve __dirname, '..', 'core', 'toJSON'
types  = require path.resolve __dirname, '..', 'core', 'types'
Q      = require 'q'

qls =

  propAll: """
    SELECT * FROM prop WHERE 1=1
  """

  propByPk: """
    SELECT * FROM prop WHERE id = ?
  """

  propCount: """
    SELECT COUNT(*) AS count FROM prop WHERE ?
  """

  propCountUniqCodePk: """
    SELECT COUNT(*) AS count FROM prop WHERE code = ? AND id != ?
  """

  propUpdateByPk: """
    UPDATE prop SET ? WHERE id = ?
  """

  propAdd: """
    INSERT INTO prop SET ?
  """

  propDeleteByPk: """
    DELETE FROM prop WHERE id = ?
  """

  entityPropAdd: """
    INSERT INTO entity_prop SET ?
  """

  entityPropValueDeleteByPropPk: """
    DELETE FROM entity_prop_value WHERE prop_id = ?
  """

  entityPropDeleteByPropPk: """
    DELETE FROM entity_prop WHERE prop_id = ?
  """

  propTypeAll: """
    SELECT * FROM prop_type WHERE 1=1
  """

  propTypeDeleteByPropPk: """
    DELETE FROM prop_type WHERE prop_id = ?
  """

class Controller

  # список всех свойств
  @ls: (request, response) ->
    self = @
    db = @db

    db.query qls.propTypeAll

    .then (res) ->
      res[0]

    .then (types) ->
      db.query qls.propAll

      .then (res) ->
        records = res[0]
        links = {};

        for key, record of records
          links[record.id] = key

        for type in types
          link = links[type.prop_id]

          if typeof records[link].type is 'undefined'
            records[link].type = []

          records[link].type.push type.type

        records

    .fail (err) ->
      self.error err

    .done (data) ->
      self.end data

  # получение свойства по его первичному ключу
  @pk: (request, response) ->
    self = @

    id = parseInt @params[1]

    @db.query qls.propByPk, [id]

    .then (res) ->
      self.end res[0].pop()

    .fail (err) ->
      self.error err

  # добавить свойство
  @mk: (request, response) ->
    self = @
    db = @db

    # get post data
    @body()

    # convert body from JSON to Object
    .then (json) ->
      toJSON json

    # validate
    .then (body) ->

      body.code = body.code.substr 0, 30
      body

    .then (body) ->
      db.beginTransaction()

      .then () ->
        # проверяем уникальность кода свойства
        db.query qls.propCount, {code: body.code}

      .then (res) ->
        # свойство с таким кодом уже существует
        if res[0][0].count > 0
          throw 'Already exists'

        # добавляем свойство в БД
        db.query qls.propAdd, {code: body.code}

      .then (res) ->
        # Первичный ключ новой записи
        new_prop_id = res[0].insertId

        # добавляем связку сущности со свойством
        db.query qls.entityPropAdd,
          prop_id: new_prop_id
          entity_id: body.entity_id

        .then (res) ->
          # коммитим запросы (транзакцию)
          db.commit().then -> {
            id: new_prop_id
          }

      .fail (err) ->
        db.rollback().then -> throw err

    .fail (err) ->
      self.error err

    .done (data) ->
      self.end data

  # изменить свойство
  @ee: (request, response) ->
    self = @
    db = @db

    id = parseInt @params[1]

    # get post data
    @body()

    # convert body from JSON to Object
    .then (json) ->
      toJSON json

    # validate
    .then (body) ->

      body.code = body.code.substr 0, 30
      body

    .then (body) ->
      db.beginTransaction()

      .then () ->
        # проверяем существование записи по первичному ключу
        db.query qls.propCount, {id: id}

      .then (res) ->
        # запись не найдена
        unless res[0][0].count
          throw 'Not exists'

        # проверяем уникальность свойства "code" у найденой записи
        db.query qls.propCountUniqCodePk, [body.code, id]

      .then (res) ->
        # свойство "code" уникальное
        if res[0][0].count > 0
          throw "A property with code '#{body.code}' already exists."

        # обновляем данные записи
        db.query qls.propUpdateByPk, [{ code: body.code }, id]

      .then (res) ->
        # коммитим запросы (транзакция)
        db.commit()

      .then ->
        # возвращаем уже обновленную запись
        db.query qls.propByPk, [id]

      .then (res) ->
        # передаем данные полученной записи в переменную
        record = res[0].pop()

      .fail (err) ->
        db.rollback().then -> throw err

    .fail (err) ->
      self.error err

    .done (data) ->
      self.end data

  # удалить свойство
  @rm: (request, response) ->
    self = @
    db = @db

    id = parseInt @params[1]

    # проверяем существование записи по первичному ключу
    db.query qls.propCount, {id: id}

    .then (res) ->
      # запись не найдена
      unless res[0][0].count
        throw 'Not exists'

    .then ->
      # получаем запись по первичному ключу
      db.query qls.propByPk, [id]

    .then (res) ->
      # передаем данные полученной записи в переменную
      record = res[0]

      db.beginTransaction()

      .then ->
        db.query qls.entityPropValueDeleteByPropPk, [id]

      .then ->
        db.query qls.entityPropDeleteByPropPk, [id]

      .then ->
        db.query qls.propTypeDeleteByPropPk, [id]

      .then ->
        db.query qls.propDeleteByPk, [id]

      .then ->
        # коммитим запросы (транзакция)
        db.commit()

      .then ->
        # возвращаем данные уже удаленной записи (на всякий случай)
        record

      .fail (err) ->
        db.rollback().then -> throw err

    .fail (err) ->
      self.error err

    .done (data) ->
      self.end data

module.exports = Controller