# display debug info
dbg = (text) ->
  date = new Date
  hours = ("0" + (date.getUTCHours())).slice(-2)
  minutes = ("0" + (date.getUTCMinutes())).slice(-2)
  seconds = ("0" + (date.getUTCSeconds())).slice(-2)
  console.log  hours + ":" + minutes + ":" + seconds + " " + text

module.exports = dbg