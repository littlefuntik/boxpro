path           = require 'path'
http           = require 'http'
Q              = require 'q'
dbg            = require path.resolve __dirname, 'dbg'
DocumentHelper = require path.resolve __dirname, 'DocumentHelper'

class Application

  routes: []
  params: null

  constructor:(@port, @host) ->
    # code here

  templateEngine: (@templateEngine) ->
  config: (@cfg) ->

  db: (db) ->
    @db =
      query: Q.nbind db.query, db
      beginTransaction: Q.nbind db.beginTransaction, db
      rollback: Q.nbind db.rollback, db
      commit: Q.nbind db.commit, db

  render: (file, params) ->
    @templateEngine.compileFile(file)(params)

  search_route: (request) ->
    result = false
    for route in @routes
      if typeof route[2] is 'function' and route[0] is request.method
        if typeof route[1] is 'string' and route[1] is request.url
          result = route
        else if route[1] instanceof RegExp
          matches = route[1].exec request.url
          if matches isnt null
            result = route
            @params = matches
    return result

  body: (request) ->
    Q.Promise (resolve, reject, notify) ->
      data = ''

      request.addListener 'data', (chunk) ->
        data += chunk
        notify data

      request.addListener 'end', ->
        resolve data

  get: (url_ptr, callback) ->
    @routes.push ['GET', url_ptr, callback]

  post: (url_ptr, callback) ->
    @routes.push ['POST', url_ptr, callback]

  delete: (url_ptr, callback) ->
    @routes.push ['DELETE', url_ptr, callback]

  run: ->

    self = @

    # server logics
    @server = http.createServer (request, response) ->

      dbg "[#{request.method}] #{request.url}"

      doc = new DocumentHelper response

      route = self.search_route request

      # маршрут найден
      if route
        doc.params = self.params
        doc.body = (body) -> self.body(request, body)
        doc.cfg = self.cfg
        doc.render = -> self.render.apply(self, arguments)
        doc.db = self.db
        route[2].call doc, request, response

      # маршрут не найден
      else
        doc.error '404 Not found'

    # run server
    @server.listen @port, @host

    # additional information
    dbg "Application Started on http://#{@host}:#{@port}/"
    dbg "Static files shared on #{self.cfg.static_url}"

module.exports = Application