Q = require 'q'

# String to JSON object
toJSON = (text) ->
  Q.Promise (resolve, reject, notify) ->
    try
      store = JSON.parse(text)
      resolve store
    catch
      reject new Error "#{_error}"

module.exports = toJSON