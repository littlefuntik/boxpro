os = require 'os'

class DocumentHelper
  status: 200
  contentType: 'text/html'
  charset: 'utf-8'

  constructor:(@response) ->

  end: (data) ->
    if typeof data is 'string'
      content = data
    else if typeof data is 'object'
      @contentType = 'application/json'
      content = JSON.stringify(data)
    else
      content = ''
    if typeof @response is 'object'
      @response.writeHead @status,
        'Content-Type': "#{@contentType}; charset=#{@charset}"
      @response.end content + os.EOL
    else
      console.log 'Unknown response object!'

  error: ->
    @status = 500
    @end.apply @, arguments

module.exports = DocumentHelper