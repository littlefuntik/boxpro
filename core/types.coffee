Q = require 'q'

Type_Number =

  name: 'Number'

  validate: (value) ->

    Q.Promise (resolve, reject, notify) ->

      switch typeof value

        when 'number'
          resolve value

        when 'string'
          resolve parseInt value

        else
          do reject



Type_String =

  name: 'String'

  validate: (value) ->

    Q.Promise (resolve, reject, notify) ->

      switch typeof value

        when 'number'
          resolve value.toString()

        when 'string'
          resolve parseInt value

        else
          do reject


types = []
types[0] = Type_Number
types[1] = Type_String

module.exports = types