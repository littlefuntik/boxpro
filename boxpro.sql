-- MySQL dump 10.13  Distrib 5.5.33, for FreeBSD9.1 (amd64)
--
-- Host: localhost    Database: boxpro
-- ------------------------------------------------------
-- Server version	5.5.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entity`
--

DROP TABLE IF EXISTS `entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity`
--

LOCK TABLES `entity` WRITE;
/*!40000 ALTER TABLE `entity` DISABLE KEYS */;
INSERT INTO `entity` VALUES (1,'lenovo_z570');
/*!40000 ALTER TABLE `entity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_prop`
--

DROP TABLE IF EXISTS `entity_prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_prop` (
  `entity_id` int(11) DEFAULT NULL,
  `prop_id` int(11) DEFAULT NULL,
  KEY `entity_id` (`entity_id`),
  KEY `prop_id` (`prop_id`),
  CONSTRAINT `entity_prop_ibfk_1` FOREIGN KEY (`prop_id`) REFERENCES `prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entity_prop_ibfk_3` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связь сущностей со свойствами';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_prop`
--

LOCK TABLES `entity_prop` WRITE;
/*!40000 ALTER TABLE `entity_prop` DISABLE KEYS */;
INSERT INTO `entity_prop` VALUES (1,1);
/*!40000 ALTER TABLE `entity_prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_prop_value`
--

DROP TABLE IF EXISTS `entity_prop_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_prop_value` (
  `entity_id` int(11) DEFAULT NULL,
  `prop_id` int(11) DEFAULT NULL,
  `prop_value_1_id` int(11) DEFAULT NULL,
  KEY `entity_id` (`entity_id`),
  KEY `prop_id` (`prop_id`),
  KEY `prop_value_1_id` (`prop_value_1_id`),
  CONSTRAINT `entity_prop_value_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entity` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entity_prop_value_ibfk_2` FOREIGN KEY (`prop_id`) REFERENCES `prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `entity_prop_value_ibfk_3` FOREIGN KEY (`prop_value_1_id`) REFERENCES `prop_value_1` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Значение свойства сущности';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_prop_value`
--

LOCK TABLES `entity_prop_value` WRITE;
/*!40000 ALTER TABLE `entity_prop_value` DISABLE KEYS */;
INSERT INTO `entity_prop_value` VALUES (1,1,1);
/*!40000 ALTER TABLE `entity_prop_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop`
--

DROP TABLE IF EXISTS `prop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Свойства';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop`
--

LOCK TABLES `prop` WRITE;
/*!40000 ALTER TABLE `prop` DISABLE KEYS */;
INSERT INTO `prop` VALUES (1,'manufacturer');
/*!40000 ALTER TABLE `prop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_type`
--

DROP TABLE IF EXISTS `prop_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_type` (
  `prop_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '0 - numeric, 1 - text',
  KEY `prop_id` (`prop_id`),
  CONSTRAINT `prop_type_ibfk_1` FOREIGN KEY (`prop_id`) REFERENCES `prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы свойств';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_type`
--

LOCK TABLES `prop_type` WRITE;
/*!40000 ALTER TABLE `prop_type` DISABLE KEYS */;
INSERT INTO `prop_type` VALUES (1,1);
/*!40000 ALTER TABLE `prop_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prop_value_1`
--

DROP TABLE IF EXISTS `prop_value_1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prop_value_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Значения свойств с типом 1';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prop_value_1`
--

LOCK TABLES `prop_value_1` WRITE;
/*!40000 ALTER TABLE `prop_value_1` DISABLE KEYS */;
INSERT INTO `prop_value_1` VALUES (1,'Lenovo');
/*!40000 ALTER TABLE `prop_value_1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-19 23:08:49
