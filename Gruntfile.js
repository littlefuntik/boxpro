module.exports = function(grunt) {

    // 1. Вся настройка находится здесь
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        copy: {
            main: {
                files: [
                    // makes all src relative to cwd
                    {
                        expand: true,
                        cwd: 'static/bower_components/bootstrap/dist/fonts',
                        src: ['**'],
                        dest: 'static/dist/fonts'
                    }
                ],
            },
        },

        watch: {
            scripts: {
                files: ['**/*.coffee', '**/*.styl'],
                tasks: ['copy', 'coffee', 'stylus', 'cssmin', 'concat', 'uglify', 'watch'],
                options: {
                    spawn: false,
                },
            },
        },

        stylus: {
            compile: {
                files: {
                    'static/css/boxpro.css': ['static/css/boxpro.styl']
                }
            }
        },

        cssmin: {
            combine: {
                files: {
                    'static/dist/css/stylesheet.min.css': [
                        'static/bower_components/bootstrap/dist/css/bootstrap.css',
                        'static/bower_components/bootstrap/dist/css/bootstrap-theme.css',

                        'static/bower_components/angular-xeditable/dist/css/xeditable.css',

                        'static/css/boxpro.css'
                    ]
                }
            }
        },

        coffee: {
            compile: {
                files: {
                    'static/js/ng-app.js': ['static/js/ng-app.coffee'],
                    'app.js': ['app.coffee'],

                    'controllers/IndexCtrl.js': ['controllers/IndexCtrl.coffee'],
                    'controllers/PropCtrl.js': ['controllers/PropCtrl.coffee'],
                    'controllers/TypeCtrl.js': ['controllers/TypeCtrl.coffee'],

                    'core/Application.js': ['core/Application.coffee'],
                    'core/DocumentHelper.js': ['core/DocumentHelper.coffee'],
                    'core/dbg.js': ['core/dbg.coffee'],
                    'core/toJSON.js': ['core/toJSON.coffee'],
                    'core/types.js': ['core/types.coffee']
                }
            }
        },

        concat: {
            dist: {
                src: [
                    'static/bower_components/angular/angular.js',

                    'static/bower_components/angular-resource/angular-resource.js',

                    'static/bower_components/angular-xeditable/dist/js/xeditable.js',

                    'static/bower_components/angular-bootstrap/ui-bootstrap.js',
                    'static/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',

                    'static/js/ng-app.js'
                ],
                dest: 'static/dist/js/javascript.js',
            }
        },

        uglify: {
            build: {
                src: 'static/dist/js/javascript.js',
                dest: 'static/dist/js/javascript.min.js'
            }
            // ,options: {
            //     report: 'min',
            //     mangle: false
            // }
        }

    });

    // 3. Тут мы указываем Grunt, что хотим использовать этот плагин
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-contrib-copy');

    // 4. Указываем, какие задачи выполняются, когда мы вводим «grunt» в терминале
    grunt.registerTask('default', ['copy', 'coffee', 'stylus', 'cssmin', 'concat', 'uglify', 'watch']);

};
