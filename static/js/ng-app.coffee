# Класс, на основе которого будут создаваться рессурсы для базовых CRUD-операций
# см. ngResurse для AngularJS
class StorageBase

  constructor:(@$resource) ->

    actions =
      'delete':
          method: 'DELETE'
          params:
            id: '@id'

    return @$resource @url, id: '@id', actions

# Рессурс для "Свойств"
class Property extends StorageBase

  constructor:(@$resource) ->

    # url, по которому будут выполняться операции с данными
    @url = '/api/property/:id'

    return super

# Рессурс для "Типов"
class Type extends StorageBase

  constructor:(@$resource) ->

    # url, по которому будут выполняться операции с данными
    @url = '/api/type/:id'

    return super

# Выполняется после инициализации Angular-приложения
class BoxproRun

  @$inject: ['$rootScope', 'Type', 'editableOptions']

  constructor: ($rootScope, Type, editableOptions) ->

    editableOptions.theme = 'bs3'

    $rootScope.types = []

    Type.query (data) ->
      $rootScope.types = data
      return

# Контроллер для списка свойств
class PropertyCtrl

  @$inject: ['$scope', '$rootScope', 'Property']

  constructor: ($scope, $rootScope, Property) ->

    $scope.props = []

    Property.query (data) ->
      $scope.props = data
      return

    $scope.insertProperty = (item) ->
      Property.save(
        item
        (data) ->
          Property.get id: data.id, (item) ->
            $scope.props.push item
        () ->
          console.log arguments
      )

    $scope.deleteProperty = (item) ->
      if confirm "Подтвердите удаление свойства №#{item.id} '#{item.code}'"
        promise = item.$delete()
        promise.then(
          (data) ->
            $scope.props.splice( $scope.props.indexOf(item), 1 );
          () ->
            console.log arguments
        )

    $scope.insertFormDisplay = 0

    $scope.editFormDisplay = []

app = angular.module 'webapp', ['ngResource', 'xeditable', 'ui.bootstrap']

app.config [
    '$interpolateProvider'
    ($interpolateProvider) ->
      $interpolateProvider.startSymbol '[['
      $interpolateProvider.endSymbol ']]'
      return
  ]

app.factory 'Property', ['$resource', Property]
app.factory 'Type', ['$resource', Type]
app.run BoxproRun
app.controller 'PropertyCtrl', PropertyCtrl
